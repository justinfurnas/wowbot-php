<?php

include __DIR__.'/vendor/autoload.php';
include __DIR__.'/vendor/justinfurnas/php-wow/PHPWow.php';

#require_once(__DIR__.'/vendor/justinfurnas/php-wow/defines.inc.php');

ini_set('display_errors','On');

$config['api_key'] = 'wgvwjh4etznu5kqd3h5mqbb7vrd848ru';
$config['locale'] = 'en_US';

$loop = React\EventLoop\Factory::create();
$client = new \CharlotteDunois\Yasmin\Client(array(), $loop);

$client->on('error', function ($error) {
    echo $error.PHP_EOL;
});

$client->on('ready', function () use ($client) {
    echo 'Logged in as '.$client->user->tag.' created on '.$client->user->createdAt->format('d.m.Y H:i:s').PHP_EOL;
});

$client->on('message', function ($message) {
    // echo 'Received Message from '.$message->author->tag.' in '.($message->channel->type === 'text' ? 'channel #'.$message->channel->name : 'DM').' with '.$message->attachments->count().' attachment(s) and '.\count($message->embeds).' embed(s)'.PHP_EOL;

	// We don't want to trigger responses from the bot, so let's only respond to users (and only if it's in a direct message)

    if (!$message->author->bot && $message->channel->type == "dm") {
    	$params = explode(' ',trim($message->content));

        switch ($params[0]) {
        	case '.help':
        		$reply = "Use `.help <command>` for help with a specific command.\r\n";
        		$reply .= "**List of Commands**\r\n";
        		$reply .= "All parameters denoted inside [ and ] are optional. Default region is set to **US**\r\n\r\n";
        		$reply .= "**Character Commands**\r\n";
        		$reply .= "`.profile <Character Name> <Realm> [region]` --- Grab the profile of the specified character\r\n";
        		$reply .= "`.professions <Character Name> <Realm> [region] [primary|secondary]` --- Grab the professions of the specified character\r\n";
        		$reply .= "\r\n";
        		$reply .= "**Dungeon Commands**\r\n";
        		$reply .= "`.affixes [region]` --- Get a list of the weekly Mythic+ affixes\r\n";
        		$reply .= "\r\n";
        		$reply .= "**Guild Commands**\r\n";
        		$reply .= "`.members <Guild Name> <Server>` --- Gather a list of members in the specified guild\r\n";
        		$reply .= "\r\n";
        		$reply .= "**General Commands\r\n";
        		$reply .= "`nothing implemented yet`\r\n";
        		break;
        	case '.profile':
        	  if (empty($params[1]) || empty($params[2])) {
        	  	$reply = INVALID_SYNTAX .' Syntax: .profile <Character> <Realm>';
        	  }
        	  else {
        	  	$character = new Character($params[1],$params[2]);
        	  	
        	  	if (!empty($character->getLevel())) {
        	  		$level = $character->getLevel();
        	  		$class = $character->lookupClass($character->getClass());
        	  		$specialization = $character->getSpecialization();
        	  		$guild = $character->getGuild();
        	  		$picture = "https://render-us.worldofwarcraft.com/character/". $character->getAvatar();
        	  		$reply = "Character found.\r\n";
        	  		$options = [
        	  			'embed' => [
        	  				"title" => "{$params[1]} - {$params[2]}",
        	  				"timestamp" => "2018-09-08T18:43:48.973Z",
        	  				"thumbnail" => [
        	  					"url" => $picture,
        	  				],
        	  				"fields" => [
        	  					[
        	  						"name" => "Profile",
        	  						"value" => "{$level} {$specialization} {$class}"
    	  						],
        	  				],
						    "footer" => [
					      		"icon_url" => "https://orig00.deviantart.net/65e3/f/2014/207/e/2/official_wow_icon_by_benashvili-d7sd1ab.png",
						      	"text" => "Courtesy Blizzard Community API"
					      	]  
        	  			],
        	  		];

        	  		if (isset($guild)) {
        	  			$options['embed']['fields'][] = [
        	  				"name" => "Guild",
        	  				"value" => "Member of **<{$guild}>**"
        	  			];
        	  		}
        	  	}
        	  	else {
        	  		$reply = CHARACTER_NOT_FOUND;
        	  	}
        	  }
        	  break;
    	  	case '.affixes':
    	 		if (!isset($params[1])) {
    	 			$params[1] = 'US';
    	 		}
    	 		if (!isset($params[2])) {
    	 			$params[2] = 'en_US';
    	 		}

    	  		$affixes = getAffixes($params[1],$params[2]);
    	  		if (isset($affixes)) {
	  				$reply = "**Weekly Mythic+ Affixes for {$params[1]}**\r\n";
    	  			foreach ($affixes as $affix) {
		  				$reply .= "**{$affix->name}**\r\n";
		  				$reply .= "{$affix->description}\r\n\r\n";
    	  			}
    	  		}
    	  		else {
    	  			$reply = AFFIXES_NOT_FOUND;
    	  		}
    	  		break;
        	default:
              $reply = COMMAND_NOT_FOUND;
              break;
        }

	    if (!empty($reply)) {
    		$reply = "\r\n" . $reply;
	    	
	    	if (isset($options)) {
			    $message->reply($reply,$options);
		    }
		    else {
		    	$message->reply($reply);
		    }
	    }
	}
});

$client->login('NDg3MTA1MDM4NzU0NTEyOTAw.DnWgRQ.QwHygoGUK8f3Zxl0zNndUINjxVI')->done();
$loop->run();

function getAffixes($region='us',$locale='en') {
	$url = "https://raider.io/api/v1/mythic-plus/affixes?region={$region}&locale={$locale}";
	$ch = curl_init($url);
	$options = array(
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HEADER => FALSE,
	);

	curl_setopt_array($ch, $options);
	$result = json_decode(curl_exec($ch));
	curl_close($ch);

	if (isset($result->affix_details)) {
		return $result->affix_details;
	}
	else {
		return NULL;
	}
}