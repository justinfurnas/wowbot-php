<?php

include __DIR__.'/vendor/autoload.php';
include __DIR__.'/vendor/justinfurnas/php-wow/PHPWow.php';

$discord = new \Discord\Discord([
    'token' => 'bot-token',
]);

$discord->on('ready', function ($discord) {
    echo "Bot is ready.", PHP_EOL;
  
    // Listen for events here
    $discord->on('message', function ($message) {
    	global $config;
    	$params = explode(' ',trim($message->content));
    	$channel = $message->channel;

    	// Parse out the commands

    	switch ($params[0]) {
    		case '.level':

	    		if (empty($params[1]) || empty($params[2])) {
	    			$reply = INVALID_SYNTAX .' Syntax: .level <character name> <realm>';
	    		}
	    		else {
	    			$character = new Character($params[1],$params[2]);
	    			$level = $character->getLevel();

	    			if (isset($level)) {
	    				$reply = "Character: {$params[1]} Level: {$character->getLevel()}";
	    			}
	    			else {
	    				$reply = CHARACTER_NOT_FOUND;
	    			}
	    		}
	    		break;
    		case '.reputation':
    		case '.profession':
    		case '.talents':
    		case '.specialization':

				if (empty($params[1]) || empty($params[2])) {
	    			$reply = INVALID_SYNTAX .' Syntax: .specialization <character name> <realm> [primary|secondary]';
	    		}
	    		else {
	    			$character = new Character($params[1],$params[2]);
	    			$specialization = $character->getSpecialization(isset($params[3]) ? $params[3] : '');

	    			if (isset($specialization)) {
	    				$reply = "Character: {$params[1]} Specialization: {$specialization}";
	    			}
	    			else {
	    				$reply = CHARACTER_NOT_FOUND;
	    			}
	    		}
	    		case '.help':
	    		 // Do the stuff here to enable
	    		case '.event':
		  			switch ($params[1]) {
	    		  	case 'add':
	    		  		// Do the functional stuff here to add the event to the system
	    		  	case 'view':
	    		  		// Do the functional stuff here to view events
	    		  	case 'delete':
	    		  	   // Do the functional stuff here to delete events
	    		  }
	    		  break;
	    		case '.reminder':
	    			switch ($params[1]) {
		    		  	case 'add':
		    		  		// Do the functional stuff here to add the event to the system
		    		  	case 'view':
		    		  		// Do the functional stuff here to view events
		    		  	case 'delete':
		    		  	   // Do the functional stuff here to delete events
	    		  }
    		  break;
    	}

		// Does the configuration say that it should only respond to private messages?
    	if ($config['is_private']) {
				if (isset($reply)) {
	    			$message->channel->sendMessage($reply); 
				}
    	}
    	// If it shouldn't only respond to private messages, what channels should it respond to?
    	elseif (isset($channel->name) && in_array($config['channels'],$channel->name)) {
    		if (isset($reply)) {
    			$message->channel->sendMessage($reply); 
				}
    	}
    });
});

$discord->run();
